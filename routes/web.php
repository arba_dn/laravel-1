<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home'); //Default Router, NamaController@NamaFunction

Route::get('/register', 'AuthController@register'); //Nama Router, NamaController@NamaFunction

Route::post('/welcome', 'AuthController@welcome'); //Nama Router, NamaController@NamaFunction, POST untuk Form


Route::get('/table', 'TableController@table'); //Nama Router, NamaController@NamaFunction

Route::get('/data-tables', 'DatatablesController@datatables'); //Nama Router, NamaController@NamaFunction

Route::get('/cast/create', 'CastController@create'); //form tambah

Route::post('/cast', 'CastController@store'); //Tambah data ke Database

Route::get('/cast', 'CastController@index'); //Nama Router, NamaController@NamaFunction

Route::get('/cast/{cast_id}', 'CastController@show'); //Detail Data

Route::get('/cast/{cast_id}/edit', 'CastController@edit'); //form edit
Route::put('/cast/{cast_id}', 'CastController@update'); //edit data ke database

Route::delete('/cast/{cast_id}', 'CastController@destroy'); //delet data ke database