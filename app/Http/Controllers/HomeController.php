<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home() // Nama Function
    {
        return view('halaman.home'); //view: Nama Folder.Nama File view
    }
}
