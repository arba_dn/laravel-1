<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() // Nama Function
    {
        return view('halaman.register'); //view: Nama Folder.Nama File view
    }

    public function welcome(Request $request)
    {
        $first_name=$request['first_name']; //value['index dari name=""yang ada di form']
        $last_name=$request['last_name']; //value['index dari name=""yang ada di form']

        return view('halaman.welcome', compact('first_name','last_name'));
    }
}
