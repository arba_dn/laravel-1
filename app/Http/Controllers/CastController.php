<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.create');
    }


    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        DB::table('cast')->insert( //Tambah Data
            ['nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]
    );

        return redirect('/cast');

    }


    public function index() //Tampil Data
    {
        $casts = DB::table('cast')->get();

        return view('cast.index', compact('casts'));
    }


    public function show($id) //Detail Data
    {
        $casts = DB::table('cast')->where('id',$id)->first();

        return view('cast.show', compact('casts'));
    }

    public function edit($id)
    {
        $casts = DB::table('cast')->where('id',$id)->first();
        
        return view('cast.edit', compact('casts'));
    }

    public function update(Request $request, $id)
    {
       $request->validate([
        'nama' => 'required',
        'umur' => 'required',
        'bio' => 'required',
    ]);

       DB::table('cast')
       ->where('id', $id)
       ->update([
        'nama' => $request['nama'],
        'umur' => $request['umur'],
        'bio' => $request['bio'],
    ]);

       return redirect('/cast');
   }


   public function destroy($id)
   {

    DB::table('cast')->where('id', '=', $id)->delete();
    return redirect('/cast');
}



}
