<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Form Regis</title>
</head>
<body>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>SanberBook</title>
</head>
<body>
	<h2>Buat Account Baru!</h2>
	<h4>Sign Up Form</h4>

	<form action="/welcome" method="post">
		@csrf
		<div>
			<label>First name:</label>
			<br>
			<input type="text" name="first_name">
		</div>

		<br>

		<div>
			<label>Last Name:</label>
			<br>
			<input type="text" name="last_name">
		</div>

		<br>

		<div>
			<label>Gender:</label>
			<br>
			<input type="radio" name="gender" value="Male">Male
			<br>
			<input type="radio" name="gender" value="Female">Female
			<br>
			<input type="radio" name="gender" value="Other">Other
		</div>

		<br>

		<div>
			<label>Nationality:</label>
			<br>
			<select name="national">
				<option value="Indonesia">Indonesian</option>
				<option value="Malaysian">Malaysian</option>
				<option value="Australian">Australian</option>
				<option value="Other">Other</option>
			</select>
		</div>

		<br>

		<div>
			<label>Language Spoken:</label>
			<br>
			<input type="checkbox" name="language" value="Bahasa Indonesia">Bahasa Indonesia
			<br>
			<input type="checkbox" name="language" value="English">English
			<br>
			<input type="checkbox" name="language" value="Other">Other
		</div>

		<br>

		<div>
			<label>Bio:</label>
			<br>
			<textarea name="bio"></textarea>
		</div>
		<br>
		<input type="submit" name="submit" value="Sign Up">
	</form>

</body>
</html>
</body>
</html>