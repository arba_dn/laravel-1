@extends('layout.master')

@section('judul')
Edit Data {{$casts->nama}}
@endsection

@section('isi')

<form action="/cast/{{$casts->id}}" method="POST">
  @csrf
  @method('put')
  <div class="card-body">
    <div class="form-group">
      <label for="exampleInputEmail1">Nama Cast</label>
      <input type="text" class="form-control" name="nama" value="{{$casts->nama}}">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label for="exampleInputPassword1">Umur</label>
      <input type="text" class="form-control" name="umur" value="{{$casts->umur}}">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <div class="form-group">
      <label for="exampleInputPassword1">Bio</label>
      <textarea name="bio" rows="5" cols="30" class="form-control" >{{$casts->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    
  </div>
  <!-- /.card-body -->

  <div class="card-footer">
    <button type="submit" class="btn btn-primary">Update</button>
  </div>
</form>
@endsection