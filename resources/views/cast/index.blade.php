@extends('layout.master')

@section('judul')
Halaman Data Cast
@endsection

@section('isi')
<a href="/cast/create" class="btn btn-primary mb-3">Tambah Cast</a>

<div class="card">

  <!-- /.card-header -->
  <div class="card-body">
    <table id="example1" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th>Nama</th>
          <th>Umur</th>
          <th>Bio</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        @forelse ($casts as $key => $item)
        <tr>
          <td>{{$key + 1}}</td>
          <td>{{$item->nama}}</td>
          <td>{{$item->umur}}</td>
          <td>{{$item->bio}}</td>
          <td>
            
            <form action="/cast/{{$item->id}}" method="POST">
              @csrf
              @method('delete')
              <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
              <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
              <input type="submit" class="btn btn-danger btn-sm" value="Delete">
            </form>
          </td>

        </tr>
        @empty

        @endforelse
      </tbody>
      <tfoot>
        <tr>
          <th>#</th>
          <th>Nama</th>
          <th>Umur</th>
          <th>Bio</th>
          <th>Action</th>
        </tr>
      </tfoot>
    </table>
  </div>
  <!-- /.card-body -->
</div>
@endsection