@extends('layout.master')

@section('judul')
Detail Cast
@endsection

@section('isi')



<div class="card-body">


  <div class="form-group">
    <label for="exampleInputPassword1">Nama Cast</label>
    <input type="text" class="form-control" value="{{$casts->nama}}" disabled>
  </div>

  <div class="form-group">
    <label for="exampleInputPassword1">Umur</label>
    <input type="text" class="form-control" value="{{$casts->umur}}" disabled>
  </div>
  

  <div class="form-group">
      <label for="exampleInputPassword1">Bio</label>
      <textarea rows="5" cols="30" class="form-control" >{{$casts->bio}}</textarea>
    </div>



</div>
<!-- /.card-body -->

<div class="card-footer">
  <a href="/cast" class="btn btn-primary btn-sm">Back</a>
</div>

@endsection