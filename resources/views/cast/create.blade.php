@extends('layout.master')

@section('judul')
Tambah Data Cast
@endsection

@section('isi')

<form action="/cast" method="POST">
  @csrf
  <div class="card-body">
    <div class="form-group">
      <label for="exampleInputEmail1">Nama Cast</label>
      <input type="text" class="form-control" name="nama">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label for="exampleInputPassword1">Umur</label>
      <input type="text" class="form-control" name="umur">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <div class="form-group">
      <label for="exampleInputPassword1">Bio</label>
      <textarea name="bio" rows="5" cols="30" class="form-control"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    
  </div>
  <!-- /.card-body -->

  <div class="card-footer">
    <button type="submit" class="btn btn-primary">Submit</button>
  </div>
</form>
@endsection